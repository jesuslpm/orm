﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using LLBLGenProLite51;
using LLBLGenProLite51.FactoryClasses;
using LLBLGenProLite51.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace LLBLGenProLite51.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TestTable. </summary>
	public partial class TestTableRelations
	{
		/// <summary>CTor</summary>
		public TestTableRelations()
		{
		}

		/// <summary>Gets all relations of the TestTableEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTestTableRelations
	{

		/// <summary>CTor</summary>
		static StaticTestTableRelations()
		{
		}
	}
}
