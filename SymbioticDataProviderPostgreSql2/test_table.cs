
namespace SymbioticDataProviderPostgreSql2 {

using FrozenElephant.Symbiotic;

using System;
using System.Diagnostics;
[Serializable()]
[DatabaseTable("test_table")]
public  class test_table 
{

    #region Private Memebers
    private string m_name;
    private int m_id;

    #endregion
    [DatabaseColumnAttribute("name", isPrimaryKey: false, isIdentityColumn: false)]
	/// <summary>
	/// Gets or sets name.
	/// </summary>
    public string name {
	    get { return m_name; }
	    set { m_name = value; }
    }

    [DatabaseColumnAttribute("id", isPrimaryKey: true, isIdentityColumn: true)]
	/// <summary>
	/// Gets or sets id.
	/// </summary>
    public int id {
	    get { return m_id; }
	    set { m_id = value; }
    }



}
}