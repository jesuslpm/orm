﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using ServiceStack.DataAnnotations;
using ServiceStack.Logging;
using ServiceStack.OrmLite;

//using log4net;
//using log4net.Config;
//SELECT * FROM pg_stat_activity where application_name = 'OrmLitetest' and datname = 'test_ormlite'  order by query_start desc;

namespace OrmLite
{
    internal class OrmLitetest
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";


        private static void Main(string[] args)
        {
            /*
            -- this is sample db table which you are playing around with:
            -- Table: test_table

            -- DROP TABLE test_table;

            CREATE TABLE test_table
            (
              name text,
              id serial NOT NULL,
              CONSTRAINT test_table_pkey PRIMARY KEY (id)
            )
            WITH (
              OIDS=FALSE
            );
            ALTER TABLE test_table
              OWNER TO postgres;

            */
            //Install-Package Npgsql -Version 3.1.10
            //  removed Install-Package log4net -Version 2.0.7
            //Uninstall-Package log4net
            //Install-Package ServiceStack.OrmLite.PostgreSQL -Version 4.5.6
            //Install-Package CsvHelper -Version 2.16.3
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(OrmLitetest));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(OrmLitetest) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(OrmLitetest) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();
#if DEBUG
            LogManager.LogFactory = new ConsoleLogFactory();
#endif
            var dbFactory =
                new OrmLiteConnectionFactory(
                    ConfigurationManager.ConnectionStrings["PostgreeDbContext"].ConnectionString,
                    PostgreSqlDialect.Provider);

            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                long initialRowCount = 0;

                using (var db = dbFactory.Open())
                {
                    /* this is fired only first time on open.Note:  It is not recorded by LogFactory
                SELECT ns.nspname, a.typname, a.oid, a.typrelid, a.typbasetype,
CASE WHEN pg_proc.proname='array_recv' THEN 'a' ELSE a.typtype END AS type,
CASE
WHEN pg_proc.proname='array_recv' THEN a.typelem
WHEN a.typtype='r' THEN rngsubtype
ELSE 0
END AS elemoid,
CASE
WHEN pg_proc.proname IN ('array_recv','oidvectorrecv') THEN 3    -- Arrays last 
WHEN a.typtype='r' THEN 2                                      -- Ranges before 
WHEN a.typtype='d' THEN 1                                       -- Domains before 
ELSE 0                                                           -- Base types first
END AS ord
FROM pg_type AS a
JOIN pg_namespace AS ns ON (ns.oid = a.typnamespace)
JOIN pg_proc ON pg_proc.oid = a.typreceive
LEFT OUTER JOIN pg_type AS b ON (b.oid = a.typelem)
LEFT OUTER JOIN pg_range ON (pg_range.rngtypid = a.oid) 
WHERE
(
  a.typtype IN ('b', 'r', 'e', 'd') AND
  (b.typtype IS NULL OR b.typtype IN ('b', 'r', 'e', 'd'))  -- Either non-array or array of supported element type 
)
               */

                    initialRowCount = db.Count<TestTableRow>();
                    //in db:
                    //SELECT COUNT(*) FROM "public"."test_table"
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var insertCollection = new List<TestTableRowWithoutId>();
                for (var i = 1; i <= numberofrecords; i++)
                {
                    //var tb = new TestTableRow {Id = i, Name = i.ToString()}; 
                    //I need TestTableRowWithoutId because Id is int and set to zero if I skip any value. And even InsertUsingDefaults fails
                    var tb = new TestTableRowWithoutId {Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                    insertCollection.Add(tb);
                    //no query is fired here
                }

                var rowCreate = new CsvRow();


                using (var db = dbFactory.Open())
                    // this doesn't mean actual queries to DB are fired at the end of using block.
                {
                    var insertArray = insertCollection.ToArray();
                    //log.InfoFormat();
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    // db.InsertAll(insertCollection);// not working with Sequence test_table_id_seq, not even with id set as 0
                    db.InsertUsingDefaults(insertArray);
                    //indb:
                    // for each record example:
                    //DEBUG: SQL: INSERT INTO "public"."test_table" ("name") VALUES (:name) PARAMS: :name=1_Ex_Cycle:1
                    //then commit
                    stopWatch.Stop();
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                }
                stopWatch.Reset();
                createCollection.Add(rowCreate);


                List<TestTableRow> allrows;
                var rowRetrieve = new CsvRow();
                using (var db2 = dbFactory.Open())
                {
                    //log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    allrows = db2.Select<TestTableRow>();
                    //in db:
                    //SELECT "id", "name" FROM "public"."test_table"
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                }
                stopWatch.Reset();
                retrieveCollection.Add(rowRetrieve);


                //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                var fstRecord = allrows.OrderBy(x => x.Id).First();
                var lastRecord = allrows.OrderBy(x => x.Id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                foreach (var item in allrows)
                {
                    item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                }

                var rowUpdate = new CsvRow();
                using (var db3 = dbFactory.Open())
                {
                    //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    db3.UpdateAll(allrows);
                    //in db:
                    //for each row like:
                    //UPDATE "public"."test_table" SET "name"=:name WHERE "id"=:id
                    //PARAMS: :id=3, :name=3_Ex_Cycle:1_2017-03-18-19-53-21-9231
                    //and then a commit

                    stopWatch.Stop();
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                }
                stopWatch.Reset();
                updateCollection.Add(rowUpdate);

                List<TestTableRow> allrowsAgain;
                using (var db4 = dbFactory.Open())
                {
                    allrowsAgain = db4.Select<TestTableRow>();
                    //in db:
                    //SELECT "id", "name" FROM "public"."test_table"
                }

                //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                var fstRecord2 = allrowsAgain.OrderBy(x => x.Id).First();
                var lastRecord2 = allrowsAgain.OrderBy(x => x.Id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord2.Id, fstRecord2.Name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord2.Id, fstRecord2.Name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord2.Id, lastRecord2.Name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord2.Id, lastRecord2.Name);

                var rowDelete = new CsvRow();
                using (var db5 = dbFactory.Open())
                {
                    //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    db5.DeleteAll(allrowsAgain);
                    //in db:
                    //DELETE FROM "public"."test_table" WHERE "id" IN (:0,:1,:2)
                    //PARAMS: :0=1, :1=2, :2=3
                    //note that all the deletes in single go nothing like commit observed
                    stopWatch.Stop();
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                }
                stopWatch.Reset();
                deleteCollection.Add(rowDelete);


                using (var db6 = dbFactory.Open())
                {
                    db6.ExecuteNonQuery("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;");
                    //in db:
                    //ALTER SEQUENCE test_table_id_seq RESTART WITH 1
                }
            }


            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //Console.ReadLine();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }


    // this is for test only otherwise you will create separate project and restructure code 
    [Alias("test_table")]
    [Schema("public")]
    public class TestTableRow
    {
        [Required]
        [Alias("id")]
        [Index(Unique = true)]
        [Sequence("test_table_id_seq")]
        public int Id { get; set; }

        [Alias("name")]
        public string Name { get; set; }
    }

    //I need TestTableRowWithoutId because Id is int and set to zero if I skip any value. And even InsertUsingDefaults fails
    [Alias("test_table")]
    [Schema("public")]
    public class TestTableRowWithoutId
    {
        [Alias("name")]
        public string Name { get; set; }
    }


    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }

    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}
