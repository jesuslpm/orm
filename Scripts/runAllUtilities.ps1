#change $appRootPath, $numberofrecords, $numberofcycles  if need be
# but make sure, all the utilities are only one level down after root apps folder
# and in one sub folder there is only one exe. Since rest will be skipped.
$appRootPath = "C:\891\orm\orm\Scripts\apps";
$numberofrecords = "5000";
$numberofcycles = "10";
Write-Host "You want to execute numberofrecords per cycle:" $numberofrecords;
Write-Host "You want to execute numberofcycles:" $numberofcycles;
Write-Host "You said all exe's are in folder:" $appRootPath;
Write-Host "Creating log folder if not exist";
If (Test-Path -Path "c:\logs" -PathType Container){
Write-Host "c:\logs already exists" -ForegroundColor Red;
}
ELSE{
 New-Item -Path "c:\logs"  -ItemType directory;
 }


$scriptdirFullName = (Get-Item -Path ".\" -Verbose).FullName;
Write-Host "You have started executing the .ps1 in folder:" $scriptdirFullName;
#Remove-Item "c:\logs\*" -recurse;
Remove-Item "c:\logs\*_test_create.csv";
Remove-Item "c:\logs\*_test_delete.csv";
Remove-Item "c:\logs\*_test_retrieve.csv";
Remove-Item "c:\logs\*_test_update.csv";
cd $appRootPath;
Get-ChildItem -dir | ForEach-Object { 
Write-Host "switching to folder: "  $_.FullName;
cd $_.FullName;
Write-Host "Current Folder is" (Get-Item -Path ".\" -Verbose).FullName;
$exeName = "";
$exeShortName = "";
$exeName = (Get-ChildItem -filter "*.exe" | Select-Object -First 1).FullName;
$exeShortName = (Get-ChildItem -filter "*.exe" | Select-Object -First 1).Name;
Write-Host "Utility Name is : "  $exeShortName;
&$exeName $numberofrecords $numberofcycles ;
Write-Host "Execution of "  $exeShortName "is over. Now sleep for 1 minute."
Start-Sleep -s 60;
}
Write-Host "Switch back to folder where .ps1 started:" $scriptdirFullName;
cd $scriptdirFullName;
