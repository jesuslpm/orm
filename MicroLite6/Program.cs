﻿/*
    -- this is sample db table which you are playing around with: If you choose to have crazy names of the tables and rows, see mapping class errors mentioned as comments near mapping class below 
    -- Table: TestTableRows

      -- DROP TABLE "TestTableRows";

    CREATE TABLE "TestTableRows"
    (
      "Name" text,
      "Id" serial NOT NULL PRIMARY KEY
      
    )
    WITH (
      OIDS=FALSE
    );
    ALTER TABLE "TestTableRows"
      OWNER TO postgres;

    */
//Install-Package MicroLite -Version 6.3.2
//Install-Package Npgsql -Version 3.1.10
//Install-Package log4net -Version 2.0.7
//Install-Package CsvHelper -Version 2.16.3
//removing log4net, if u wish u may restore and add back commented code, ( also in app.config)
//Uninstall-Package log4net


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using MicroLite;
using MicroLite.Configuration;
using MicroLite.Mapping;
using MicroLite.Mapping.Attributes;

//using log4net;
//using log4net.Config;

namespace MicroLite6
{
    internal class MicroLite6Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(MicroLite6Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(MicroLite6Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(MicroLite6Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();

            var sessionFactory = Configure
                .Fluently()
                .ForPostgreSqlConnection("PostgreeDbContext")
                .CreateSessionFactory();

            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = 0;
                using (var ses = sessionFactory.OpenSession())
                {
                    var seqQuery = new SqlQuery("SELECT COUNT(*) FROM public.\"TestTableRows\";");
                    initialRowCount = ses.Advanced.ExecuteScalar<int>(seqQuery);
                    //in db:
                    //SELECT COUNT(*) FROM public."TestTableRows"
                    // to get above actions in db the query is:
                    //SELECT * FROM pg_stat_activity where application_name = 'MicroLite6Test' and datname = 'test_MicroLite'  order by query_start desc;
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var rowCreate = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                        rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                        for (var i = 1; i <= numberofrecords; i++)
                        {
                            var tb = new TestTableRow {Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                            stopWatch.Start();
                            ses.Insert(tb);
                            //on each Insert call below, since sequence is used, 
                            //in db last command after this is:
                            //SELECT lastval()
                            stopWatch.Stop();
                        }
                        stopWatch.Start();
                        tarns.Commit();
                        //in db: actual rows commited here
                        stopWatch.Stop();
                        rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                        rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    }
                }
                stopWatch.Reset();
                createCollection.Add(rowCreate);


                var rowRetrieve = new CsvRow();
                var rowUpdate = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        var retrieveQuery = new SqlQuery("SELECT \"Id\", \"Name\" FROM public.\"TestTableRows\"; ");
                        //log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                        rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                        stopWatch.Start();
                        var allrows = ses.Fetch<TestTableRow>(retrieveQuery);
                        stopWatch.Stop();
                        rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                        rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();


                        //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                        Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                        var fstRecord = allrows.OrderBy(x => x.Id).First();
                        var lastRecord = allrows.OrderBy(x => x.Id).Last();
                        //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                        Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                        //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                        Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                        //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                        rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);

                        foreach (var item in allrows)
                        {
                            item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                            stopWatch.Start();
                            ses.Update(item);
                            //in db: 
                            //"TestTableRows" SET "Name" = $1 WHERE ("Id" = $2)
                            // but transaction is completed at commit
                            stopWatch.Stop();
                        }

                        
                        stopWatch.Start();
                        tarns.Commit();
                        //in db:
                        //commited here.
                        stopWatch.Stop();
                        rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                        rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();
                    }
                }
                retrieveCollection.Add(rowRetrieve);
                updateCollection.Add(rowUpdate);

                var rowDelete = new CsvRow();
                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        var retrieveQuery = new SqlQuery("SELECT \"Id\", \"Name\" FROM public.\"TestTableRows\"; ");
                        var allrowsAgain = ses.Fetch<TestTableRow>(retrieveQuery);
                        //already recorded retrive time in update above, so skipping here


                        //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                        Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                        var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                        var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                        //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                        Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                        //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                        Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                        //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                        Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                        rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                        foreach (var item in allrowsAgain)
                        {
                            stopWatch.Start();
                            ses.Delete(item);
                            //in db: DELETE FROM "TestTableRows" WHERE ("Id" = $1)
                            //but transaction is completed at commit
                            stopWatch.Stop();
                        }

                        
                        stopWatch.Start();
                        tarns.Commit();
                        //in db:
                        //committed here
                        stopWatch.Stop();
                        rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                        //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                        Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                        rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                        stopWatch.Reset();
                    }
                }
                deleteCollection.Add(rowDelete);


                using (var ses = sessionFactory.OpenSession())
                {
                    using (var tarns = ses.BeginTransaction())
                    {
                        var seqQuery = new SqlQuery("ALTER SEQUENCE \"TestTableRows_Id_seq\" RESTART WITH 1;");
                        ses.Advanced.Execute(seqQuery);
                        // strange that this didn't wait for trans commit in contrast to other places above
                        tarns.Commit();
                    }
                }
                //   select * from pg_stat_activity where datname = 'test_MicroLite';

//select * from public."TestTableRows";
            }

            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //Console.ReadLine();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    /*
     * I started with table like 
    CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
   
   mapping as commented below gives me error
    42P01: relation "TestTableRows" does not exist


  MicroLite.MicroLiteException was unhandled
    HResult=-2146233088
    Message=42P01: relation "TestTableRows" does not exist
    Source=MicroLite
    StackTrace:
         at MicroLite.Core.Session.ExecuteScalarQuery[T](SqlQuery sqlQuery)
         at MicroLite.Core.Session.InsertReturningIdentifier(IObjectInfo objectInfo, Object instance)
         at MicroLite.Core.Session.Insert(Object instance)
         at MicroLite6.MicroLite6Test.Main(String[] args) in c:\891\MicroLite6\Program.cs:line 58
         at System.AppDomain._nExecuteAssembly(RuntimeAssembly assembly, String[] args)
         at System.AppDomain.ExecuteAssembly(String assemblyFile, Evidence assemblySecurity, String[] args)
         at Microsoft.VisualStudio.HostingProcess.HostProc.RunUsersAssembly()
         at System.Threading.ThreadHelper.ThreadStart_Context(Object state)
         at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
         at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
         at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state)
         at System.Threading.ThreadHelper.ThreadStart()
    InnerException: Npgsql.PostgresException
         HResult=-2147467259
         Message=42P01: relation "TestTableRows" does not exist
         Source=Npgsql
         ErrorCode=-2147467259
         BaseMessage=relation "TestTableRows" does not exist
         Code=42P01
         File=parse_relation.c
         InternalPosition=0
         Line=1159
         MessageText=relation "TestTableRows" does not exist
         Position=13
         Routine=parserOpenTable
         Severity=ERROR
         SqlState=42P01
         StackTrace:
              at Npgsql.NpgsqlConnector.DoReadMessage(DataRowLoadingMode dataRowLoadingMode, Boolean isPrependedMessage)
              at Npgsql.NpgsqlConnector.ReadMessageWithPrepended(DataRowLoadingMode dataRowLoadingMode)
              at Npgsql.NpgsqlConnector.ReadMessage(DataRowLoadingMode dataRowLoadingMode)
              at Npgsql.NpgsqlConnector.ReadExpecting[T]()
              at Npgsql.NpgsqlDataReader.NextResultInternal()
              at Npgsql.NpgsqlDataReader.NextResult()
              at Npgsql.NpgsqlCommand.Execute(CommandBehavior behavior)
              at Npgsql.NpgsqlCommand.ExecuteScalarInternal()
              at Npgsql.NpgsqlCommand.ExecuteScalar()
              at MicroLite.Core.Session.ExecuteScalarQuery[T](SqlQuery sqlQuery)
         InnerException: 
     * 
     * The mapping in my code was:
      [Table(schema: "public", name: "test_table")]
      public class TestTableRow
      {
        
        
          [Column("id")]
          [Identifier(IdentifierStrategy.Sequence, "test_table_id_seq")]
          public int Id { get; set; }

          [Column("name")]
          public string Name { get; set; }
      }
     * or I tried but same error
     [Table("public", "test_table")]
    public class TestTableRow
    {


        [Column("id")]
        [Identifier(IdentifierStrategy.Sequence, "test_table_id_seq")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
    * or I tried but same error
     [Table("test_table")]
    public class TestTableRow
    {


        [Column("id")]
        [Identifier(IdentifierStrategy.Sequence, "test_table_id_seq")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
     * 
    So I was forced to switch table to like below:
     * 
     *    -- DROP TABLE "TestTableRows";

    CREATE TABLE "TestTableRows"
    (
      "Name" text,
      "Id" serial NOT NULL PRIMARY KEY
      
    )
    WITH (
      OIDS=FALSE
    );
    ALTER TABLE "TestTableRows"
      OWNER TO postgres;
     */

    public class TestTableRow
    {
        [Identifier(IdentifierStrategy.Sequence, "TestTableRows_Id_seq")]
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}
