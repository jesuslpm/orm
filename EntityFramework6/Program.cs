﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

//using log4net;
//using log4net.Config;

namespace EntityFramework6
{
    internal class EntityFramework6Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            /*
           -- this is sample db table which you are playing around with:
           -- Table: test_table

           -- DROP TABLE test_table;

           CREATE TABLE test_table
           (
             name text,
             id serial NOT NULL,
             CONSTRAINT test_table_pkey PRIMARY KEY (id)
           )
           WITH (
             OIDS=FALSE
           );
           ALTER TABLE test_table
             OWNER TO postgres;

           */
            //Install-Package log4net -Version 2.0.7
            //Uninstall-Package log4net
            //Install-Package CsvHelper -Version 2.16.3
            //Install-Package Npgsql -Version 3.1.10
            //Install-Package EntityFramework -Version 6.1.3
            //Install-Package EntityFramework6.Npgsql -Version 3.1.1

            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(EntityFramework6Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(EntityFramework6Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(EntityFramework6Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();
            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = 0;
                using (var db0 = new DB())
                {
                    initialRowCount = db0.TestTable.Count();
                    /*
                     Opened connection at 3/16/2017 3:27:45 PM -04:00
SELECT "GroupBy1"."A1" AS "C1" FROM (SELECT CAST (count(1) AS int4) AS "A1" FROM
 "public"."test_table" AS "Extent1") AS "GroupBy1"
-- Executing at 3/16/2017 3:27:46 PM -04:00
-- Completed in 26 ms with result: NpgsqlDataReader

Closed connection at 3/16/2017 3:27:46 PM -04:00
                     */
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var rowCreate = new CsvRow();
                using (var db = new DB())
                {
                    for (var i = 1; i <= numberofrecords; i++)
                    {
                        var tb = new TestTableRow {Name = string.Format("{0}_Ex_Cycle:{1}",i,executioncycle)};
                        db.TestTable.Add(tb);
                    }
                    // log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    db.SaveChanges(); //actual db query for create update and delete is fired on save changes
                    /*
                     Started transaction at 2/23/2017 10:27:59 PM -05:00
INSERT INTO "public"."test_table"("name") VALUES (@p_0) RETURNING "id"
-- p_0: '1' (Type = Object, IsNullable = false)
-- Executing at 2/23/2017 10:27:59 PM -05:00
-- Completed in 25 ms with result: NpgsqlDataReader

INSERT INTO "public"."test_table"("name") VALUES (@p_0) RETURNING "id"
-- p_0: '2' (Type = Object, IsNullable = false)
-- Executing at 2/23/2017 10:27:59 PM -05:00
-- Completed in 0 ms with result: NpgsqlDataReader

INSERT INTO "public"."test_table"("name") VALUES (@p_0) RETURNING "id"
-- p_0: '3' (Type = Object, IsNullable = false)
-- Executing at 2/23/2017 10:27:59 PM -05:00
-- Completed in 0 ms with result: NpgsqlDataReader

Committed transaction at 2/23/2017 10:27:59 PM -05:00
Closed connection at 2/23/2017 10:27:59 PM -05:00
                     */
                    stopWatch.Stop();
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                }
                stopWatch.Reset();
                createCollection.Add(rowCreate);


                List<TestTableRow> allrows;
                using (var db2 = new DB())
                {
                    var rowRetrieve = new CsvRow();
                    // log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    allrows = db2.TestTable.ToList(); // data access fired on .tolist()
                    /*
                     Opened connection at 2/23/2017 10:29:10 PM -05:00
SELECT "Extent1"."id", "Extent1"."name" FROM "public"."test_table" AS "Extent1"
-- Executing at 2/23/2017 10:29:11 PM -05:00
-- Completed in 1 ms with result: NpgsqlDataReader

Closed connection at 2/23/2017 10:29:11 PM -05:00
                     */
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    //  log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    retrieveCollection.Add(rowRetrieve);


                    //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                    Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                    var fstRecord = allrows.OrderBy(x => x.Id).First();
                    var lastRecord = allrows.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    var rowUpdate = new CsvRow();
                    //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                    foreach (var item in allrows)
                    {
                        item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                        stopWatch.Start();
                        db2.TestTable.AddOrUpdate(item);
                        //AddOrUpdate is calling db to get records too, so this overhead should be taken into account
                        /*
                    
Opened connection at 2/23/2017 10:30:04 PM -05:00
SELECT "Extent1"."id", "Extent1"."name" FROM "public"."test_table" AS "Extent1"
WHERE 5001 = "Extent1"."id" LIMIT 2
-- Executing at 2/23/2017 10:30:04 PM -05:00
-- Completed in 0 ms with result: NpgsqlDataReader

Closed connection at 2/23/2017 10:30:04 PM -05:00
                         */
                        stopWatch.Stop();
                    }
                    stopWatch.Start();
                    db2.SaveChanges(); //actual db query for create update and delete is fired on save changes
                    /*
                     Opened connection at 2/23/2017 10:33:12 PM -05:00
Started transaction at 2/23/2017 10:33:12 PM -05:00
UPDATE "public"."test_table" SET "name"=@p_0 WHERE "id" = @p_1
-- p_0: '1_2017-02-23-22-32-58-4783' (Type = Object, IsNullable = false)
-- p_1: '5004' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:33:12 PM -05:00
-- Completed in 2 ms with result: 1

UPDATE "public"."test_table" SET "name"=@p_0 WHERE "id" = @p_1
-- p_0: '2_2017-02-23-22-32-58-7073' (Type = Object, IsNullable = false)
-- p_1: '5005' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:33:12 PM -05:00
-- Completed in 0 ms with result: 1

UPDATE "public"."test_table" SET "name"=@p_0 WHERE "id" = @p_1
-- p_0: '3_2017-02-23-22-32-58-7153' (Type = Object, IsNullable = false)
-- p_1: '5006' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:33:12 PM -05:00
-- Completed in 0 ms with result: 1

Committed transaction at 2/23/2017 10:33:12 PM -05:00
Closed connection at 2/23/2017 10:33:12 PM -05:00
                      */
                    stopWatch.Stop();
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    updateCollection.Add(rowUpdate);
                }


                using (var db3 = new DB())
                {
                    var rowDelete = new CsvRow();
                    var allrowsAgain = db3.TestTable.ToList();

                    //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                    var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    foreach (var item in allrowsAgain)
                    {
                        db3.TestTable.Remove(item);
                    }
                    //  log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    db3.SaveChanges(); //actual db query for create update and delete is fired on save changes
                    /*
                     Opened connection at 2/23/2017 10:34:43 PM -05:00
Started transaction at 2/23/2017 10:34:43 PM -05:00
DELETE FROM "public"."test_table" WHERE "id" = @p_0
-- p_0: '5004' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:34:43 PM -05:00
-- Completed in 1 ms with result: 1

DELETE FROM "public"."test_table" WHERE "id" = @p_0
-- p_0: '5005' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:34:43 PM -05:00
-- Completed in 0 ms with result: 1

DELETE FROM "public"."test_table" WHERE "id" = @p_0
-- p_0: '5006' (Type = Int32, IsNullable = false)
-- Executing at 2/23/2017 10:34:43 PM -05:00
-- Completed in 0 ms with result: 1

Committed transaction at 2/23/2017 10:34:43 PM -05:00
Closed connection at 2/23/2017 10:34:43 PM -05:00
                      */
                    stopWatch.Stop();
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    deleteCollection.Add(rowDelete);
                }

                using (var db4 = new DB())
                {
                    db4.Database.ExecuteSqlCommand("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;");
                    /*
                     Opened connection at 2/23/2017 10:35:19 PM -05:00
Started transaction at 2/23/2017 10:35:19 PM -05:00
ALTER SEQUENCE test_table_id_seq RESTART WITH 1;
-- Executing at 2/23/2017 10:35:19 PM -05:00
-- Completed in 0 ms with result: -1

Committed transaction at 2/23/2017 10:35:19 PM -05:00
Closed connection at 2/23/2017 10:35:19 PM -05:00
                     */
                }
            }

            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //  Console.ReadLine();
        }


        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }

    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }

    [Table("test_table", Schema = "public")]
    public class TestTableRow
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }

    public class DB : DbContext
    {
        public DB()
            : base("PostgreeDbContext")
        {
            /*
             * started.
SELECT "GroupBy1"."A1" AS "C1" FROM (SELECT CAST (count(1) AS int4) AS "A1" FROM
 "dbo"."__MigrationHistory" AS "Extent1") AS "GroupBy1"
-- Executing at 2/8/2017 11:15:32 PM -05:00
-- Failed in 62 ms with error: 42P01: relation "dbo.__MigrationHistory" does not
 exist

SELECT "GroupBy1"."A1" AS "C1" FROM (SELECT CAST (count(1) AS int4) AS "A1" FROM
 "dbo"."__MigrationHistory" AS "Extent1") AS "GroupBy1"
-- Executing at 2/8/2017 11:15:32 PM -05:00
-- Failed in 55 ms with error: 42P01: relation "dbo.__MigrationHistory" does not
 exist
 To avoid this  turn off the initializer as mentioned in line below
             */
            Database.SetInitializer<DB>(null);
#if DEBUG
            Database.Log = Console.Write;
#endif
        }

        public DbSet<TestTableRow> TestTable { get; set; }
    }
}
