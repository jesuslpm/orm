﻿//Install-Package Npgsql -Version 2.2.7
//install-Package EntityLite -Version 1.12.1
/*
           -- this is sample db table which you are playing around with:
           -- Table: test_table

           -- DROP TABLE test_table;

           CREATE TABLE test_table
           (
             name text,
             id serial NOT NULL,
             CONSTRAINT test_table_pkey PRIMARY KEY (id)
           )
           WITH (
             OIDS=FALSE
           );
           ALTER TABLE test_table
             OWNER TO postgres;

           */

using System;
using System.Reflection;
namespace i_nercya_EntityLite_1121_generate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(AssemblyName.GetAssemblyName(@"C:\891\i_nercya_EntityLite_1121_generate\packages\Npgsql.2.2.7\lib\net35\Mono.Security.dll"));
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(AssemblyName.GetAssemblyName(@"C:\891\i_nercya_EntityLite_1121_generate\packages\Npgsql.2.2.7\lib\net35\Npgsql.dll"));
            Console.ReadLine();
        }
    }
}
