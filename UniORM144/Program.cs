﻿/*
          -- this is sample db table which you are playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

          */
//Install-Package Uni.Extensions -Version 1.1.9 - this I had to update later , see comments in main
//Install-Package Uni.ORM -Version 1.4.4- this I had to update later , see comments in main
//Install-Package Npgsql -Version 3.1.10
// Install-Package CsvHelper -Version 2.16.3
// Install-Package log4net -Version 2.0.7
//uninstall-package log4net

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using Npgsql;
using Uni.Orm;

//using log4net;
//using log4net.Config;

namespace UniORM144
{
    internal class UniORM144Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(UniORM144Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(UniORM144Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(UniORM144Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();

            var postgresql = new UniOrm("PostgreeDbContext", NpgsqlFactory.Instance);

            //  var result = postgresql.dyno.Query(Schema: "public", Table: "test_table");
            /* I got below mentioned error at var result = postgresql.dyno.Query(Schema: "public", Table: "test_table");
          System.MissingFieldException was unhandled
HResult=-2146233071
Message=Field not found: 'Uni.Extensions.UniExtensions.stringType'.
Source=Uni.Orm
StackTrace:
     at Uni.Orm.UniOrm.TryInvokeMember(InvokeMemberBinder binder, Object[] args, Object& result)
     at CallSite.Target(Closure , CallSite , Object , String , String )
     at System.Dynamic.UpdateDelegates.UpdateAndExecute3[T0,T1,T2,TRet](CallSite site, T0 arg0, T1 arg1, T2 arg2)
     at UniORM144.UniORM144Test.Main(String[] args) in c:\891\UniORM144\Program.cs:line 35
     at System.AppDomain._nExecuteAssembly(RuntimeAssembly assembly, String[] args)
     at System.AppDomain.ExecuteAssembly(String assemblyFile, Evidence assemblySecurity, String[] args)
     at Microsoft.VisualStudio.HostingProcess.HostProc.RunUsersAssembly()
     at System.Threading.ThreadHelper.ThreadStart_Context(Object state)
     at System.Threading.ExecutionContext.RunInternal(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
     at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state, Boolean preserveSyncCtx)
     at System.Threading.ExecutionContext.Run(ExecutionContext executionContext, ContextCallback callback, Object state)
     at System.Threading.ThreadHelper.ThreadStart()
InnerException: 
             * 
             * Resolution:
             * uninstall-package Uni.ORM
             * uninstall-package Uni.Extensions
             * Install-Package Uni.Extensions -Version 1.1.8
             *  install-Package Uni.ORM -Version 1.4.4
             *  so, 1.1.9 version of extensions was the source of issue
           */

            /*
          //generating   poco
            var result1 = postgresql.dyno.Query(Table: "test_table", Limit: 1);

            var anonymousObj = new UniAnonymousObject();

            var tblType = anonymousObj.GetDynamicType(result1, "test_table");

            string tblPoco = anonymousObj.GetPoco(result1, "test_table");
             */

            //SELECT * FROM pg_stat_activity where application_name = 'UniORM144Test' and datname = 'test_UniORM144' order by query_start desc;
            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = postgresql.Count("public", "test_table");
                //in db:
                //SELECT COUNT(*) COUNT FROM public.test_table

                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                var insertRows = new List<test_table>();
                for (var i = 1; i <= numberofrecords; i++)
                {
                    var tb = new test_table {name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                    insertRows.Add(tb);
                }
                var insertRowsAr = insertRows.ToArray();
                var rowCreate = new CsvRow();
                // log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                var insertResult = postgresql.dyno.Insert(
                    Table: "test_table",
                    PKField: "id",
                    Args: insertRowsAr
                    );

                stopWatch.Stop();
                rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                //  log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                createCollection.Add(rowCreate);


                var rowRetrieve = new CsvRow();
                //  log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                IEnumerable<test_table> result = postgresql.dyno.Query<test_table>(Schema: "public", Table: "test_table");
                stopWatch.Stop();
                rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                //  log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                retrieveCollection.Add(rowRetrieve);


                var allRows = result.ToList();


                //  log.InfoFormat("\tRetrieved {0} records after inserts.", allRows.Count);
                Console.WriteLine("\tRetrieved {0} records after inserts.", allRows.Count);
                var fstRecord = allRows.OrderBy(x => x.id).First();
                var lastRecord = allRows.OrderBy(x => x.id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.id, fstRecord.name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.id, fstRecord.name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.id, lastRecord.name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.id, lastRecord.name);


                foreach (var item in allRows)
                {
                    item.name = string.Format("{0}_{1}", item.name, DateTime.Now.ToString(dateTimeFormat));
                }
                var allRowsAr = allRows.ToArray();
                var rowUpdate = new CsvRow();
                // log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                var bulkUpdateResult = postgresql.dyno.Update(
                    Table: "test_table",
                    Columns: "name",
                    Args: allRowsAr
                    );
                stopWatch.Stop();
                rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                // log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                updateCollection.Add(rowUpdate);


                IEnumerable<test_table> resultAgain = postgresql.dyno.Query<test_table>(Schema: "public", Table: "test_table");
                var allrowsAgain = result.ToList();

                //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                var fstRecordAgain = allrowsAgain.OrderBy(x => x.id).First();
                var lastRecordAgain = allrowsAgain.OrderBy(x => x.id).Last();
                //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecordAgain.id, fstRecordAgain.name);
                Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecordAgain.id, fstRecordAgain.name);
                //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecordAgain.id, lastRecordAgain.name);
                Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecordAgain.id, lastRecordAgain.name);



                // var allRowsIdAr = allRows.Select(x => x.id).ToArray(); int[] won't work
                var allRowsIdAr = allrowsAgain.Select(x => x.id as object).ToArray();
                var rowDelete = new CsvRow();
                //  log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                stopWatch.Start();
                var deleteResult = postgresql.dyno.Delete(Table: "test_table", PKField: "id", Args: allRowsIdAr);
                stopWatch.Stop();
                rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                stopWatch.Reset();
                deleteCollection.Add(rowDelete);


                postgresql.ExecuteNonQuery(CommandType.Text, "public",
                    commandText: "ALTER SEQUENCE test_table_id_seq RESTART WITH 1", args: null);
            }
            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //Console.ReadLine();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class test_table
    {
        public string name { get; set; }

        public int id { get; set; }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}
