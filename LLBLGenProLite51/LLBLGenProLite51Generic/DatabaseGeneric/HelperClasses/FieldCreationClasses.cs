﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 5.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using LLBLGenProLite51.FactoryClasses;
using LLBLGenProLite51;

namespace LLBLGenProLite51.HelperClasses
{
	/// <summary>Field Creation Class for entity TestTableEntity</summary>
	public partial class TestTableFields
	{
		/// <summary>Creates a new TestTableEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(TestTableFieldIndex.Id);}
		}
		/// <summary>Creates a new TestTableEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(TestTableFieldIndex.Name);}
		}
	}
	

}