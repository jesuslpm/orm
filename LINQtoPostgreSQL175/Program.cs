﻿/*
          -- this is sample db table which you are playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

          */
//Install-Package Npgsql -Version 3.1.10
//Install-Package linq2db -Version 1.7.6
//Install-Package linq2db.t4models -Version 1.7.6
//Install-Package linq2db.PostgreSQL -Version 1.7.6
//Install-Package log4net -Version 2.0.7
//Install-Package CsvHelper -Version 2.16.3
//Uninstall-Package log4net


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using LinqToDB;
using LinqToDB.Data;
using LinqToDB.Mapping;

//using log4net.Config;
//using log4net;

namespace LINQtoPostgreSQL17x
{
    internal class LINQtoPostgreSQL17xTest
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";
        //I have given ApplicationName in app.config>connection string to check the actual queries
        //SELECT * FROM pg_stat_activity where application_name = 'LINQtoPostgreSQL17xTest' and datname = 'test_LINQtoPostgreSQL' order by query_start desc;
        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(LINQtoPostgreSQL17xTest));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(LINQtoPostgreSQL17xTest) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(LINQtoPostgreSQL17xTest) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();
            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = 0;
                /* on start of a using block the query fired:
                
                   SELECT ns.nspname, a.typname, a.oid, a.typrelid, a.typbasetype,
CASE WHEN pg_proc.proname='array_recv' THEN 'a' ELSE a.typtype END AS type,
CASE
  WHEN pg_proc.proname='array_recv' THEN a.typelem
  WHEN a.typtype='r' THEN rngsubtype
  ELSE 0
END AS elemoid,
CASE
  WHEN pg_proc.proname IN ('array_recv','oidvectorrecv') THEN 3    -- Arrays last 
  WHEN a.typtype='r' THEN 2                                        -- Ranges before  
  WHEN a.typtype='d' THEN 1                                        -- Domains before 
  ELSE 0                                                           -- Base types first 
END AS ord
FROM pg_type AS a
JOIN pg_namespace AS ns ON (ns.oid = a.typnamespace)
JOIN pg_proc ON pg_proc.oid = a.typreceive
LEFT OUTER JOIN pg_type AS b ON (b.oid = a.typelem)
LEFT OUTER JOIN pg_range ON (pg_range.rngtypid = a.oid) 
WHERE
  (
    a.typtype IN ('b', 'r', 'e', 'd') AND
    (b.typtype IS NULL OR b.typtype IN ('b', 'r', 'e', 'd'))  -- Either non-array or array of supported element type 
  )
                     */
                using (var db = new TestLINQtoPostgreSQLDB())
                {
                    var qRetrieve =
                        from c in db.test_table
                        select c;
                    initialRowCount = qRetrieve.Count();
                    /*
                         SELECT  Count(*) as cnt FROM "public".test_table t1
                         */
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                using (var db = new TestLINQtoPostgreSQLDB())
                {
                    var rowCreate = new CsvRow();
                    var insertRows = new List<TestTableRow>();
                    for (var i = 1; i <= numberofrecords; i++)
                    {
                        var tb = new TestTableRow {Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                        insertRows.Add(tb);
                    }
                    //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    db.BulkCopy(insertRows);
                    //query in db like:
                    //INSERT INTO "public".test_table (name) VALUES ('1_Ex_Cycle:1'), ('2_Ex_Cycle:1'), ('3_Ex_Cycle:1')
                    stopWatch.Stop();
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    createCollection.Add(rowCreate);
                }
                using (var db = new TestLINQtoPostgreSQLDB())
                {
                    var rowRetrieve = new CsvRow();
                    var qRetrieve =
                        from c in db.test_table
                        select c;
                    //log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    var allrows = qRetrieve.ToList();
                    //query in db like : SELECT t1.name, 	t1.id FROM 	"public".test_table t1
                   
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    retrieveCollection.Add(rowRetrieve);


                    //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                    Console.WriteLine("\tRetrieved {0} records after inserts.", allrows.Count);
                    var fstRecord = allrows.OrderBy(x => x.Id).First();
                    var lastRecord = allrows.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                    var rowUpdate = new CsvRow();
                    //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                  
                    foreach (var item in allrows)
                    {
                        item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                        stopWatch.Start();
                        db.Update(item);
                        //in db:
                       // UPDATE "public".test_table SET name = $1 WHERE "public".test_table.id = $2
                        stopWatch.Stop();
                    }
                    /*
                    qRetrieve.Update(
                        x =>
                            new TestTableRow
                            {
                                Id = x.Id,
                                Name = x.Name + "_" + DateTime.Now.ToString(dateTimeFormat)
                            });
                    //query in db like:
                    //UPDATE	"public".test_table SET	id = "public".test_table.id,	name = "public".test_table.name || '_' || $1
                   
                     */
                    
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    updateCollection.Add(rowUpdate);
                }
                using (var db = new TestLINQtoPostgreSQLDB())
                {
                    /* this is valid alternative code for logging to console with explanation
                    var qRetrieveAll =
                        from c in db.test_table
                        select c;
                    var qRetrievefirst =
                        from c in db.test_table
                        orderby c.Id ascending
                        select c;
                    var qRetrievelast =
                        from c in db.test_table
                        orderby c.Id descending
                        select c;
                    //  log.InfoFormat("\tAvailable {0} records after updates.", qRetrieveAll.Count());
                    Console.WriteLine("\tAvailable {0} records after updates.", qRetrieveAll.Count());
                    //SELECT Count(*) as cnt FROM "public".test_table t1
                    var fstRecord = qRetrievefirst.Take(1).ToList().First();
                    //command in Postgresql:
                    //SELECT t1.name, t1.id FROM "public".test_table t1 ORDER BY t1.id LIMIT 1
                    var lastRecord = qRetrievelast.Take(1).ToList().First();
                    //command in Postgresql:
                    //SELECT t1.name, t1.id FROM "public".test_table t1 ORDER BY t1.id DESC LIMIT 1
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                      */
                    var qRetrieveAll =
                        from c in db.test_table
                        select c;
                    var allrowsAgain = qRetrieveAll.ToList();
                    //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                    var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);

                    var rowDelete = new CsvRow();
                    //log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    qRetrieveAll.Delete();
                    //command in Postgresql:
                    //DELETE FROM "public".test_table t1
                    stopWatch.Stop();
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    deleteCollection.Add(rowDelete);


                    db.Execute("ALTER SEQUENCE test_table_id_seq RESTART WITH 1;");
                    //command in Postgresql:
                    //ALTER SEQUENCE test_table_id_seq RESTART WITH 1
                }
            }
            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            // Console.ReadLine();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }


    //How data model genrated ? refer to ORM's owners instriuctions at : https://youtu.be/Qc-5UpMYQO0

    //---------------------------------------------------------------------------------------------------
    // <auto-generated>
    //    This code was generated by T4Model template for T4 (https://github.com/linq2db/t4models).
    //    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
    // </auto-generated>
    //---------------------------------------------------------------------------------------------------



    /// <summary>
    /// Database       : test_LINQtoPostgreSQL
    /// Data Source    : localhost
    /// Server Version : 9.5.4
    /// </summary>
    public partial class TestLINQtoPostgreSQLDB : LinqToDB.Data.DataConnection
    {
        public ITable<TestTableRow> test_table { get { return this.GetTable<TestTableRow>(); } }

        public TestLINQtoPostgreSQLDB()
        {
            InitDataContext();
        }

        public TestLINQtoPostgreSQLDB(string configuration)
            : base(configuration)
        {
            InitDataContext();
        }

        partial void InitDataContext();
    }
//notice that test_table_id_seq is not being mentioned here
    [Table(Schema = "public", Name = "test_table")]
    public partial class TestTableRow
    {
        [Column("name"), Nullable]
        public string Name { get; set; } // text
        [Column("id"), PrimaryKey, Identity]
        public int Id { get; set; } // integer
    }

    public static partial class TableExtensions
    {
        public static TestTableRow Find(this ITable<TestTableRow> table, int Id)
        {
            return table.FirstOrDefault(t =>
                t.Id == Id);
        }
    }
}