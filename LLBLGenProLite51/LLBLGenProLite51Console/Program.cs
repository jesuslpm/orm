﻿
 /*
          -- this is sample db table which I am playing around with:
          -- Table: test_table

          -- DROP TABLE test_table;

          CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

*/
////Projects LLBLGenProLite51Generic and LLBLGenProLite51DBSpecific are generated using "C:\Program Files (x86)\Solutions Design\LLBLGen Pro v5.1\LLBLGenPro.exe"
////Version 5.1 (5.1.2) RTM - LITE  Build Date 24-Jan-2017, Licensee: LITE user
////available at llblgen.com/Pages/Lite.aspx

////Install-Package Npgsql -Version 3.1.10
/// Install-Package CsvHelper -Version 2.16.3
/// Install-Package log4net -Version 2.0.7
/// Add refrence to SD.LLBLGen.Pro.ORMSupportClasses.dll
/// 
/// while debug, uncomment system.diagnostics section in app.config
/// Also, 
/// SELECT * FROM pg_stat_activity where application_name = 'LLBLGenProLite51Test' and datname = 'test_LLBLGenProLite51' and application_name != 'pgAdmin III - Query Tool' order by query_start desc;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using LLBLGenProLite51.DatabaseSpecific;
using LLBLGenProLite51.EntityClasses;
using LLBLGenProLite51.HelperClasses;
using LLBLGenProLite51.Linq;

//using log4net;
//using log4net.Config;

namespace LLBLGenProLite51Console
{
    internal class LLBLGenProLite51Test
    {
        private const string dateTimeFormat = "yyyy-MM-dd-HH-mm-ss-ffff";

        private static void Main(string[] args)
        {
            //XmlConfigurator.Configure();
            //var log = LogManager.GetLogger(typeof(LLBLGenProLite51Test));
            var numberofrecords = Convert.ToInt32(args[0]);
            var numberofcycles = Convert.ToInt32(args[1]);
            // log.InfoFormat("(LLBLGenProLite51Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}", numberofrecords, numberofcycles);
            Console.WriteLine(
                "(LLBLGenProLite51Test) Number of records per cycle: {0}. Total Number of executioncycles: {1}",
                numberofrecords,
                numberofcycles);
            //log.Info("started.");
            Console.WriteLine("started.");
            var createCollection = new List<CsvRow>();
            var retrieveCollection = new List<CsvRow>();
            var updateCollection = new List<CsvRow>();
            var deleteCollection = new List<CsvRow>();
            var stopWatch = new Stopwatch();


            for (var executioncycle = 1; executioncycle <= numberofcycles; executioncycle++)
            {
                #region verify exit

                var initialRowCount = 0;
                using (var adapter = new DataAccessAdapter())
                {
                    /*
                    var allRows = new EntityCollection<TestTableEntity>();
                    initialRowCount = adapter.GetDbCount(allRows, null);
                    //query in postgresql executed:
                    //SELECT COUNT(*) AS NumberOfRows FROM (SELECT "public"."test_table"."id" AS "Id", "public"."test_table"."name" AS "Name" FROM "public"."test_table"  ) TmpResult
                    // see <system.diagnostics> tag in app.config

                     */
                    var metaData = new LinqMetaData(adapter);
                    var q = from c in metaData.TestTable
                        select c;
                    initialRowCount = q.Count();
                    //query in postgresql executed:
                    //SELECT COUNT(*) AS "LPAV_" FROM "public"."test_table"  "LPLA_1"  LIMIT 1
                }
                if (initialRowCount != 0)
                {
                    //log.InfoFormat("Row count must have been 0 at the beginning of execution cycle {0}",executioncycle);
                    Console.WriteLine("Row count must have been 0 at the beginning of execution cycle {0}",
                        executioncycle);
                    Environment.Exit(-1);
                }

                #endregion

                using (var adapter = new DataAccessAdapter())
                {
                    var instRows = new EntityCollection<TestTableEntity>();
                    for (var i = 1; i <= numberofrecords; i++)
                    {
                        var tb = new TestTableEntity {Name = string.Format("{0}_Ex_Cycle:{1}", i, executioncycle)};
                        instRows.Add(tb);
                        // adapter.SaveEntity(tb);// ignore this, just testing another way to insert
                    }
                    var rowCreate = new CsvRow();
                    //log.InfoFormat("starting create. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting create. executioncycle: {0}", executioncycle);
                    rowCreate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    adapter.SaveEntityCollection(instRows);
                    //for each row:
                    //SELECT currval('"public"."test_table_id_seq"')
                    //INSERT INTO "public"."test_table" ("id", "name") VALUES (nextval('"public"."test_table_id_seq"'), :p2)
                    stopWatch.Stop();
                    rowCreate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished creating. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished creating. executioncycle: {0}", executioncycle);
                    rowCreate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    createCollection.Add(rowCreate);
                }
                /* ignore this part, just testing another way to retrive
                //var metaData = new LinqMetaData(adapter);
                //var q = from c in metaData.TestTable
                //        select c;
                //var output = q.ToList();                   
                 */

                using (var adapter = new DataAccessAdapter())
                {
                    var allRows = new EntityCollection<TestTableEntity>();
                    var rowRetrieve = new CsvRow();
                    //log.InfoFormat("starting retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    adapter.FetchEntityCollection(allRows, null);
                    //SELECT "public"."test_table"."id" AS "Id", "public"."test_table"."name" AS "Name" FROM "public"."test_table"  
                    stopWatch.Stop();
                    rowRetrieve.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished retrieve. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished retrieve. executioncycle: {0}", executioncycle);
                    rowRetrieve.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    retrieveCollection.Add(rowRetrieve);


                    //  log.InfoFormat("\tRetrieved {0} records after inserts.", allrows.Count);
                    Console.WriteLine("\tRetrieved {0} records after inserts.", allRows.Count);
                    var fstRecord = allRows.OrderBy(x => x.Id).First();
                    var lastRecord = allRows.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                    foreach (var item in allRows)
                    {
                        item.Name = string.Format("{0}_{1}", item.Name, DateTime.Now.ToString(dateTimeFormat));
                    }
                    var rowUpdate = new CsvRow();
                    //log.InfoFormat("starting update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting update. executioncycle: {0}", executioncycle);
                    rowUpdate.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    adapter.SaveEntityCollection(allRows);
                    //for each row
                    //UPDATE "public"."test_table" SET "name"=:p1 WHERE ( "public"."test_table"."id" = :p2)
                    stopWatch.Stop();
                    rowUpdate.End = DateTime.Now.ToString(dateTimeFormat);
                    // log.InfoFormat("finished update. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished update. executioncycle: {0}", executioncycle);
                    rowUpdate.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    updateCollection.Add(rowUpdate);
                }
                using (var adapter = new DataAccessAdapter())
                {
                    var allrowsAgain = new EntityCollection<TestTableEntity>();
                    adapter.FetchEntityCollection(allrowsAgain, null);
                    //  log.InfoFormat("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    Console.WriteLine("\tRetrieved {0} records after updates.", allrowsAgain.Count);
                    var fstRecord = allrowsAgain.OrderBy(x => x.Id).First();
                    var lastRecord = allrowsAgain.OrderBy(x => x.Id).Last();
                    //  log.InfoFormat("\t\tfstRecord:: {0}||{1}", fstRecord.Id,fstRecord.Name);
                    Console.WriteLine("\t\tfstRecord:: {0}||{1}", fstRecord.Id, fstRecord.Name);
                    //  log.InfoFormat("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);
                    Console.WriteLine("\t\tlastRecord:: {0}||{1}", lastRecord.Id, lastRecord.Name);


                    var rowDelete = new CsvRow();
                    // log.InfoFormat("starting delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("starting delete. executioncycle: {0}", executioncycle);
                    rowDelete.Start = DateTime.Now.ToString(dateTimeFormat);
                    stopWatch.Start();
                    adapter.DeleteEntityCollection(allrowsAgain);
                    //for each row 
                    //DELETE FROM "public"."test_table" WHERE ( "public"."test_table"."id" = :p1)
                    stopWatch.Stop();
                    rowDelete.End = DateTime.Now.ToString(dateTimeFormat);
                    //log.InfoFormat("finished delete. executioncycle: {0}", executioncycle);
                    Console.WriteLine("finished delete. executioncycle: {0}", executioncycle);
                    rowDelete.TimeTaken = stopWatch.Elapsed.TotalMilliseconds.ToString();
                    stopWatch.Reset();
                    deleteCollection.Add(rowDelete);


                    adapter.ExecuteSQL("ALTER SEQUENCE test_table_id_seq RESTART WITH 1");
                }
            }

            CreateCSVs(createCollection, retrieveCollection, updateCollection, deleteCollection);

            Console.WriteLine("Done");
            //Console.ReadLine();
        }

        private static void CreateCSVs(List<CsvRow> createCollection, List<CsvRow> retrieveCollection,
            List<CsvRow> updateCollection,
            List<CsvRow> deleteCollection)
        {
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationcreate"]))
            {
                var csv = new CsvWriter(sw);
                csv.Configuration.RegisterClassMap<CsvRowMap>();
                csv.WriteRecords(createCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationretrieve"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(retrieveCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationupdate"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(updateCollection);
            }
            using (var sw = new StreamWriter(ConfigurationManager.AppSettings["csvLocationdelete"]))
            {
                var csv = new CsvWriter(sw);

                csv.WriteRecords(deleteCollection);
            }
        }
    }

    public class CsvRow
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string TimeTaken { get; set; }
    }


    public sealed class CsvRowMap : CsvClassMap<CsvRow>
    {
        public CsvRowMap()
        {
            AutoMap();
        }
    }
}
