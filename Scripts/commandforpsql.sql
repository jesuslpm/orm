SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname in 
('test_UniORM144', 
'test_SymbioticDataProviderPostgreSql2', 
'test_ormlite', 
'test_NHibernate', 
'test_LLBLGenProLite51', 
'test_LINQtoPostgreSQL', 
'test_InercyaEntityLite', 
'test_FluentData', 
'test_entityframework6', 
'test_MicroLite' )
  AND pid <> pg_backend_pid();

DROP DATABASE  IF EXISTS  "test_UniORM144";
DROP DATABASE  IF EXISTS  "test_SymbioticDataProviderPostgreSql2";
DROP DATABASE  IF EXISTS  "test_ormlite";
DROP DATABASE  IF EXISTS  "test_NHibernate";
DROP DATABASE  IF EXISTS  "test_LLBLGenProLite51";
DROP DATABASE  IF EXISTS  "test_LINQtoPostgreSQL";
DROP DATABASE  IF EXISTS  "test_InercyaEntityLite";
DROP DATABASE  IF EXISTS  "test_FluentData";
DROP DATABASE  IF EXISTS  "test_entityframework6";
DROP DATABASE  IF EXISTS  "test_MicroLite";


CREATE DATABASE "test_UniORM144";
CREATE DATABASE "test_SymbioticDataProviderPostgreSql2";
CREATE DATABASE "test_ormlite";
CREATE DATABASE "test_NHibernate";
CREATE DATABASE "test_LLBLGenProLite51";
CREATE DATABASE "test_LINQtoPostgreSQL";
CREATE DATABASE "test_InercyaEntityLite";
CREATE DATABASE "test_FluentData";
CREATE DATABASE "test_entityframework6";
CREATE DATABASE "test_MicroLite";

\connect "test_UniORM144"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;

\connect "test_SymbioticDataProviderPostgreSql2"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_ormlite"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_NHibernate"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_LLBLGenProLite51"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_LINQtoPostgreSQL"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_InercyaEntityLite"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_FluentData"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_entityframework6"
 CREATE TABLE test_table
          (
            name text,
            id serial NOT NULL,
            CONSTRAINT test_table_pkey PRIMARY KEY (id)
          )
          WITH (
            OIDS=FALSE
          );
          ALTER TABLE test_table
            OWNER TO postgres;
			
\connect "test_MicroLite"
   CREATE TABLE "TestTableRows"
    (
      "Name" text,
      "Id" serial NOT NULL PRIMARY KEY
      
    )
    WITH (
      OIDS=FALSE
    );
    ALTER TABLE "TestTableRows"
      OWNER TO postgres;		

		

  