echo off
echo Be prepared to enter password 11 times, once to default db, and then 10 times - once for each newly created db in sequence.
echo If you don't want to enter password so many times refer to https://www.postgresql.org/docs/current/static/libpq-pgpass.html
echo after starting psql you will give command like   \i C://bigpath//commandforpsql.sql
pause
echo on
"C:\Program Files\PostgreSQL\9.6\bin\psql.exe" -h "localhost" --username "postgres" --port "5432" --echo-all --dbname "postgres" --password

